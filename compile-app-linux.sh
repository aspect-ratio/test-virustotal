#!/bin/bash

OUTPUTDIR=./build/

mkdir -p ${OUTPUTDIR}
command -v g++ >/dev/null 2>&1 || { echo >&2 "g++ is not installed, you can install it with: \n sudo apt install g++"; exit 1; }
g++ './src/main.cpp' -o "${OUTPUTDIR}/app.linux.exec"