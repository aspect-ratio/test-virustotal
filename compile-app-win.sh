#!/bin/bash

OUTPUTDIR=./build/

mkdir -p ${OUTPUTDIR}
command -v i686-w64-mingw32-g++ >/dev/null 2>&1 || { echo >&2 "mingw-w64 is not installed, you can install it with: \n sudo apt install mingw-w64"; exit 1; }
i686-w64-mingw32-g++ -static-libgcc -static-libstdc++ './src/main1.cpp' -o "${OUTPUTDIR}/app.win1.exe"
i686-w64-mingw32-g++ -static-libgcc -static-libstdc++ './src/main2.cpp' -o "${OUTPUTDIR}/app.win2.exe"
